import { Component } from "react";
const urlOrder='http://42.115.221.44:8080/devcamp-pizza365/orders';
const urlVoucher='http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/';
const urlDrink='http://42.115.221.44:8080/devcamp-pizza365/drinks';
class FetchApi extends Component{
    fetchAPI=async(url,body)=>{
        let res=await fetch(url,body);
        let data=await res.json();
        return data;
    }
    getAllOrder=()=>{
        this.fetchAPI(urlOrder)
        .then((data)=>{
            console.log(data);
        })
    }
    getAllDrink=()=>{
        this.fetchAPI(urlDrink)
        .then((data)=>{
            console.log(data);
        })
    }
    getOrderById=()=>{
        this.fetchAPI(urlOrder+'/Cui8pwHQOr')
        .then((data)=>{
            console.log(data);
        })
    }
    getVoucherById=()=>{
        this.fetchAPI(urlVoucher+'/12332')
        .then((data)=>{
            console.log(data);
        })
    }
    createOrder=()=>{
        let body={
            method: 'POST',
            body: JSON.stringify({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "Phạm Thanh Bình",
                thanhTien: "200000",
                email: "binhpt001@devcamp.edu.vn",
                soDienThoai: "0865241654",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.fetchAPI(urlOrder,body)
        .then((data)=>{
            console.log(data);
        })
    }
    updateOrder=()=>{
        let body={
            method: 'PUT',
            body: JSON.stringify({
                trangThai: "confirmed" 
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.fetchAPI(urlOrder+'/438556',body)
        .then((data)=>{
            console.log(data);
        })
    }
    render(){
        return(
            <div className="container">
                <h1 className="text-success text-center">Fetch Api</h1>
                <div className="form-group mt-3">
                        <p id="cmt2dev">Test Page for Javascrip Tasks. F5 to run code. </p>
                </div>
                <div className="form-group">
                    
                    <form id="singleForm w-75">
                        <input onClick={this.getAllOrder} style={{margin:'0 10px 10px 10px'}} type="button" className="btn btn-primary p-2"  value="Call api get all orders!"/>
                        <input onClick={this.createOrder} style={{margin:'0 10px 10px 10px'}} type="button" className="btn btn-info p-2 text-light"  value="Call api create order!"/>
                        <input onClick={this.getOrderById} style={{margin:'0 10px 10px 10px'}} type="button" className="btn btn-success p-2"  value="Call api get order by id!"/>
                        <input onClick={this.updateOrder} style={{margin:'0 10px 10px 10px'}} type="button"  className="btn btn-secondary p-2"  value="Call api update order!"/>
                        <input onClick={this.getVoucherById} style={{margin:'0 10px 10px 10px'}} type="button"  className="btn btn-danger p-2"  value="Call api check voucher by id!"/>
                        <input onClick={this.getAllDrink} style={{margin:'0 10px 10px 10px'}} type="button"  className="btn btn-success p-2" value="Call api Get drink list!"/> 
                    </form>
                </div>
                <div className="form-group">
                    <p id="testP" > Demo 06 API for Pizza 365 Project: </p>
                    <ul>
                        <li>get all Orders: lấy tất cả orders </li>
                        <li>create Order: tạo 1 order</li>
                        <li>get Order by ID: lấy 1 order bằng ID </li>
                        <li>update Order: update 01 order</li>
                        <li>check voucher by ID: check thông tin mã giảm giá, quan trọng là có hay không, và % giảm giá </li>
                        <li>get drink list: lấy danh sách đồ uống</li>
                    </ul>
                    <strong className="text-danger"> Bật console log để nhìn rõ output </strong>
                    <input onClick={this.props.display} style={{margin:'10px 10px 10px 10px'}} type="button"  className="btn btn-success p-2" value="Axios Display"/> 
                </div>
            </div>
        )
    }
}

export default FetchApi;