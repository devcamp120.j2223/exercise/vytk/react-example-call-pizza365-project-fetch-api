import { Component } from "react";
import AxiosLibary from "./AxiosApi";
import FetchApi from "./FetchAPi";

class Display extends Component{
    constructor(props){
        super(props);
        this.state={
            display:true
        }
    }
    displayFetch = () => {
        this.setState({
            display: false
        })
    }
    displayAxios = () => {
        this.setState({
            display: true  
        })
    }
    render(){
        return(
            <div className="col-sm-12">
                {this.state.display?<FetchApi display={this.displayFetch}/> : <AxiosLibary display={this.displayAxios}/>}
            </div>
        )
    }
}
export default Display